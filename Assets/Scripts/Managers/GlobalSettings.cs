using System;
using UnityEngine;

namespace Managers
{
    public class GlobalSettings : Singleton<GlobalSettings>
    {
        public CustomColor[] colors;
    }
    
    [Serializable]
    public class CustomColor
    {
        public string name;
        public Color color;
    }
}