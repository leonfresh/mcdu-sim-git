namespace Managers
{
    public class GlobalStrings
    {
        
    }

    public struct NavStrings
    {
        public const string Return = "RETURN";
        public const string Or = "OR";
    }

    public struct Text
    {
        public const string TemperatureSign = "°C";
        public const string LeftCaretSymbol = "\U0000f0d9";
        public const string RightCaretSymbol = "\U0000f0da";
    }
   
    public struct Errors
    {
        public const string OutOfRange = "* OUT OF RANGE *";
        public const string InvalidEntry = "* INVALID ENTRY *";
    }
    
    public struct Key
    {
        public const string Prev = "PREV";
        public const string Next = "NEXT";
        public const string PlusSlashMinus = "+/-";
        public const string Del = "DEL";
        public const string Clr = "CLR";
        public const string Sp = "SP";
    }
}