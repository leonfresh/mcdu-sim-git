﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using MEC;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;

public enum ScratchPadInputType
{
    EditingName1,
    EditingName2,
    EditingBothNames
}

namespace Managers
{
    public class DisplayManager : Singleton<DisplayManager>
    {
        [Header("Scratch Pad")] public TextMeshProUGUI scratchPadTextMesh;
        public string scratchPadMemoryText;
        public int scratchPadMaxCharacterLength = 24;
        public int scratchPadMaxCharacters = 40;

        public TextMeshProUGUI titleTextMesh;
        public TextMeshProUGUI titleLeftTextMesh;

        public TextMeshProUGUI paginationTextMesh;

        public TextMeshProUGUI[] leftTextMeshes;
        public TextMeshProUGUI[] rightTextMeshes;

        public TextMeshProUGUI[] leftCaptionTextMeshes;
        public TextMeshProUGUI[] rightCaptionTextMeshes;

        public ScreenJson currentScreen;

        public ScratchPadInputType currentScratchPadInputType;
        
        public string lastScreenName;
        public int lastScreenPage;
        public string lastSelectedDropdownString;
        public int lastSelectedOrNumber;

        public int requestedPage;

        /// <summary>
        /// Starts a coroutine when the key is held down.
        /// </summary>
        /// <param name="keyString"></param>
        public void RenderScratchPad(string keyString)
        {
            InputManager.Instance.keysHeldDown.Add(keyString);
            Timing.RunCoroutine(_RenderScratchPad(keyString));
        }

        private IEnumerator<float> _RenderScratchPad(string keyString)
        {
            float count = InputManager.Instance.keyHoldDownRepeatTime;
            while (InputManager.Instance.keysHeldDown.Contains(keyString) &&
                   InputManager.Instance.keysHeldDown.Count(x => x == keyString) == 1)
            {
                count += Time.deltaTime;

                if (count > InputManager.Instance.keyHoldDownRepeatTime)
                {
                    count = 0;
                    var lastChar = (char) 0;

                    if (scratchPadMemoryText.Length >= 40 && keyString != Key.Clr)
                    {
                        yield break;
                    }

                    if (scratchPadMemoryText.Length - 1 >= 0)
                        lastChar = scratchPadMemoryText[scratchPadMemoryText.Length - 1];

                    var last = lastChar.ToString();

                    switch (keyString)
                    {
                        case Key.Clr:
                            RemoveCharFromRight();
                            break;
                        case Key.Del:
                            if (last == "-")
                            {
                                ClearScratchPad();
                            }

                            break;
                        case Key.Sp:
                            scratchPadMemoryText += (char) 160;
                            break;
                        case Key.PlusSlashMinus:
                            var newChar = "-";

                            if (last == "+" || last == "-")
                            {
                                newChar = last == "+" ? "-" : "+";

                                RemoveCharFromRight();
                            }

                            scratchPadMemoryText += newChar;

                            break;
                        default:
                            scratchPadMemoryText += keyString;
                            break;
                    }

                    WrapText();
                }

                yield return Time.deltaTime;
            }
        }

        /// <summary>
        /// Clears the Scratchpad
        /// </summary>
        private void ClearScratchPad()
        {
            scratchPadMemoryText = "";
            scratchPadTextMesh.text = "";
        }

        /// <summary>
        /// Sets the scratchpad text.
        /// </summary>
        /// <param name="text"></param>
        private void SetScratchPadText(string text)
        {
            scratchPadMemoryText = text;
            scratchPadTextMesh.text = text;
        }

        /// <summary>
        /// Removes one character from the right.
        /// </summary>
        public void RemoveCharFromRight()
        {
            if (scratchPadMemoryText.Length == 0)
                return;
            scratchPadMemoryText = scratchPadMemoryText.Remove(scratchPadMemoryText.Length - 1);
        }

        /// <summary>
        /// Wraps the text when over the character limit of the scratchpad.
        /// </summary>
        private void WrapText()
        {
            // Text doesn't need to be wrapped if it's less than the scratch pads max character length.
            if (scratchPadMemoryText.Length < scratchPadMaxCharacterLength)
            {
                scratchPadTextMesh.text = scratchPadMemoryText;
                return;
            }

            var amountToRemove = scratchPadMemoryText.Length - scratchPadMaxCharacterLength;
            scratchPadTextMesh.text = scratchPadMemoryText.Substring(amountToRemove);
        }

        /// <summary>
        /// Clears the Screen
        /// </summary>
        public void ClearScreen()
        {
            foreach (var t in leftTextMeshes)
            {
                t.text = "";
            }

            foreach (var t in rightTextMeshes)
            {
                t.text = "";
            }

            foreach (var t in leftCaptionTextMeshes)
            {
                t.text = "";
            }

            foreach (var t in rightCaptionTextMeshes)
            {
                t.text = "";
            }

            paginationTextMesh.text = "";
            titleTextMesh.text = "";
            titleLeftTextMesh.text = "";
        }

        /// <summary>
        /// Renders the text from the JSON file
        /// </summary>
        /// <param name="keyString"></param>
        /// <param name="buttonType"></param>
        /// <param name="toPage"></param>
        public void RenderScreen(string keyString, ButtonType buttonType, int toPage = 0)
        {
            requestedPage = 1;
            var requestedScreenName = keyString;

            // Check if what you pressed is pagination
            if (currentScreen != null && currentScreen.screenName.Length > 0 && buttonType == ButtonType.PrevNextKeys)
            {
                var newPage = currentScreen.currentPagination + toPage;
                if (newPage >= 1)
                {
                    if (newPage <= currentScreen.totalPagination)
                        requestedPage = newPage;
                    else
                        requestedPage = 1;
                }
                else
                {
                    requestedPage = currentScreen.totalPagination;
                }
            }

            // If we are on a screen, we can check the line-key press.
            if (currentScreen != null && !string.IsNullOrEmpty(currentScreen.screenName))
            {
                if (buttonType == ButtonType.LineKeys)
                {
                    requestedScreenName = LineKeysCheck(requestedScreenName, keyString, buttonType);
                    if (requestedScreenName == null)
                        return;
                }
            }

            var screen = JsonManager.Instance.screenJsons.FirstOrDefault(x =>
                x.screenName == requestedScreenName &&
                x.currentPagination == requestedPage);

            if (screen != null)
            {
                currentScreen = screen;
            }
            else
            {
                return;
            }

            RefreshScreen();
        }

        public void RefreshScreen()
        {
            ClearScreen();

            SetTitleText();

            SetPaginationText();

            SetAllOtherTexts();
        }

        public string LineKeysCheck(string requestedScreenName, string keyString, ButtonType buttonType)
        {
            var key = Regex.Match(keyString, @"\d+").Value;
            var keyInt = Convert.ToInt32(key) - 1;
            
            // L1, L2
            if (keyString.Contains("L"))
            {
                if (currentScreen.leftTexts.Length <= keyInt)
                    return null;

                requestedScreenName = MenuTypeCheck(currentScreen.leftTexts[keyInt], keyInt);
//                
//                if (requestedScreenName == NavStrings.Or)
//                {
//
//                    return null;
//                }
            }
            else if (keyString.Contains("R"))// R1, R2 etc
            {
                if (currentScreen.rightTexts.Length <= keyInt)
                {
                    return null;
                }
                    
//                Debug.Log(currentScreen.rightTexts[keyInt].name + " and menu type of " + currentScreen.rightTexts[keyInt].menuType);
                requestedScreenName = MenuTypeCheck(currentScreen.rightTexts[keyInt], keyInt);
            }

            return requestedScreenName;
        }

        public string MenuTypeCheck(MenuItem menuItem, int keyNumber)
        {
            switch (menuItem.menuType)
            {
                case MenuType.Text:
                    return null;
                case MenuType.Navigation:
                    if (menuItem.name == NavStrings.Or)
                    {
                        Debug.Log("OR PRESSED");
                        
                        CacheStuff(menuItem, keyNumber);
//                        ClearScreen();
                        ClearOrScreen();
                        RenderOrScreen(menuItem);
                    }
                    else if (menuItem.name == NavStrings.Return)
                    {
                        Debug.Log("RETURN PRESSED");
                        requestedPage = lastScreenPage;
                        return lastScreenName;
                    }
                    else if (menuItem.isDropdown && !string.IsNullOrEmpty(menuItem.name))
                    {
                        requestedPage = lastScreenPage;
//                        lastSelectedDropdownString = menuItem.name;
                        
                        // Change the screen
                        var screen =
                            JsonManager.Instance.screenJsons.FirstOrDefault(x => x.screenName == lastScreenName && x.currentPagination == lastScreenPage);

                        if (screen != null)
                        {
                            screen.leftTexts[lastSelectedOrNumber].name = menuItem.name;
                            Debug.Log(screen.screenName);
                            Debug.Log(menuItem.name);
                        }
                            
                        
                        return lastScreenName;
                    }
                    return menuItem.name;
                case MenuType.EditableNumber:
                case MenuType.EditableNumberAlphabet:
                case MenuType.Gauge:
                    if (scratchPadMemoryText.Length > 0)
                    {
                        EditableErrorCheck(menuItem);
                    }  
                    else if (scratchPadMemoryText.Length == 0)
                    {
                        SetScratchPadText(CombinedText(menuItem));   
                    }
                    break;
                case MenuType.Toggle:
                    ChangeToggle(menuItem);
                    break;
                default:
                    return null;
            }

            return null;
        }


        public void CacheStuff(MenuItem menuItem, int keyInt)
        {
            lastSelectedOrNumber = keyInt;
            lastScreenName = currentScreen.screenName;
            lastScreenPage = currentScreen.currentPagination;
        }

        public void ClearOrScreen()
        {
            var screen = JsonManager.Instance.screenJsons.FirstOrDefault(x =>
                x.screenName == NavStrings.Or);

            if (screen == null)
                return;

            foreach (var t in screen.leftTexts)
            {
                t.name = null;
            }

            for (var i = 1; i < screen.rightTexts.Length; i++)
            {
                var t = screen.rightTexts[i];
                t.name = null;
            }

            screen.titleName = null;
        }
        
        public void RenderOrScreen(MenuItem menuItem)
        {
            var screen = JsonManager.Instance.screenJsons.FirstOrDefault(x =>
                x.screenName == NavStrings.Or);

            if (screen == null)
                return;

            screen.titleName = menuItem.dropdownTitle;

            if (menuItem.dropdowns == null)
            {
                return;
            }
            
            for (var i = 0; i < currentScreen.leftTexts.Length && i < menuItem.dropdowns.Length; i++)
            {
                var t = screen.leftTexts[i];
                
                t.name = menuItem.dropdowns[i];
                Debug.Log(t.name);
            }

            var remaining = menuItem.dropdowns.Length - leftTextMeshes.Length;
            if (remaining > 0)
            {
                for (var i = 1; i < remaining && i < currentScreen.rightTexts.Length; i++)
                {
                    var t = screen.rightTexts[i];
                    
                    t.name = menuItem.dropdowns[i +  leftTextMeshes.Length];
                }
            }
        }
//        public void RenderOrScreen(MenuItem menuItem)
//        {
//            for (var i = 0; i < menuItem.dropdowns.Length && i < leftTextMeshes.Length; i++)
//            {
//                var t = leftTextMeshes[i];
//                
//                t.text = Text.LeftCaretSymbol + "<size=\"10%\"> </size>" + menuItem.dropdowns[i];
//            }
//            
//            rightTextMeshes[0].text = NavStrings.Return + "<size=\"10%\"> </size>" + Text.RightCaretSymbol;
//
//            var remaining = menuItem.dropdowns.Length - leftTextMeshes.Length;
//            if (remaining > 0)
//            {
//                for (var i = 1; i < remaining && i < rightTextMeshes.Length; i++)
//                {
//                    var t = rightTextMeshes[i];
//                    t.text = menuItem.dropdowns[i] + "<size=\"10%\"> </size>" + Text.RightCaretSymbol;
//                }
//            }
//
//        }

        public string CombinedText(MenuItem menuItem)
        {
            return (menuItem.prepend + menuItem.name + menuItem.append + menuItem.prepend2 + menuItem.name2 +
                    menuItem.append2).Replace("%", "").Replace(" ","").Replace("MIN","").Replace("°","");
        }

        public void ChangeToggle(MenuItem menuItem)
        {
            if (menuItem.currentActiveToggle != menuItem.toggleFields.Length - 1)
            {
                menuItem.currentActiveToggle++;
            }
            else
            {
                menuItem.currentActiveToggle = 0;
            }
            RefreshScreen();
        }

        public void EditableErrorCheck(MenuItem menuItem)
        {
            // Detect slash
            const char slash = (char) 47;
            if (scratchPadMemoryText[0] == slash)
                currentScratchPadInputType = ScratchPadInputType.EditingName2;
            else if (scratchPadMemoryText.Contains(slash) && scratchPadMemoryText[0] != slash &&
                     scratchPadMemoryText[scratchPadMemoryText.Length - 1] != slash)
                currentScratchPadInputType = ScratchPadInputType.EditingBothNames;
            else
                currentScratchPadInputType = ScratchPadInputType.EditingName1;
            
            switch (menuItem.menuType)
            {
                case MenuType.EditableNumberAlphabet:
                {
                    if (scratchPadMemoryText.All(char.IsDigit) ||
                        menuItem.minRange > 0 ||
                        menuItem.maxRange > 0)
                    {
                        DisplayError(Errors.InvalidEntry);
                    }
                    else if (scratchPadMemoryText.Length > menuItem.charLimit)
                    {
                        DisplayError(Errors.InvalidEntry);
                    }
                    else
                    {
                        menuItem.name = scratchPadMemoryText;
                        ClearScratchPad();
                        SetAllOtherTexts();
                    }

                    break;
                }
                case MenuType.Gauge:
                case MenuType.EditableNumber:
                {
                    // If there's an alphabet found
                    if (Regex.Matches(scratchPadMemoryText, @"[a-zA-Z]").Count > 0)
                    {
                        // This numeric field contains an alphabet. So lets search this string to see if it matches.

                        var newMemoryText = scratchPadMemoryText.Replace(" ", "");

                        if (!string.IsNullOrEmpty(menuItem.append))
                        {
                            var newAppend = menuItem.append.Replace(" ", "");
                            if (!newMemoryText.EndsWith(newAppend))
                            {
                                DisplayError(Errors.InvalidEntry);
                                return;
                            }
                        }

                        if (!string.IsNullOrEmpty(menuItem.prepend))
                        {
                            var newPrependField = menuItem.prepend.Replace(" ", "");
                            if (!newMemoryText.StartsWith(newPrependField))
                            {
                                DisplayError(Errors.InvalidEntry);
                                return;
                            }
                        }

                        if (string.IsNullOrEmpty(menuItem.append) && string.IsNullOrEmpty(menuItem.prepend))
                        {
                            DisplayError(Errors.InvalidEntry);
                            return;
                        }
                    }
                    
                    // Keep only numbers and .
                    var digitOnly = Helper.KeepOnlyNumbersAndDotInString(scratchPadMemoryText);

                    if (digitOnly.Length == 0)
                        break;

                    switch (currentScratchPadInputType)
                    {
                        case ScratchPadInputType.EditingName1:
                            // Check range.
                            if (!CheckRange1(menuItem, digitOnly))
                            {
                                DisplayError(Errors.OutOfRange);
                                return;
                            }
                                                                             
                            menuItem.name = digitOnly;
                            break;
                        case ScratchPadInputType.EditingName2:
                            // Check range.
                            if (!CheckRange2(menuItem, digitOnly))
                            {
                                DisplayError(Errors.OutOfRange);
                                return;
                            }

                            menuItem.name2 = digitOnly;
                            break;
                        case ScratchPadInputType.EditingBothNames:
                            var slashPosition = scratchPadMemoryText.IndexOf(slash);
                            // Work out the first name based on the slash.
                            var name1 = scratchPadMemoryText.Substring(0, slashPosition);
                            var name2 = scratchPadMemoryText.Substring(slashPosition + 1);

                            name1 = Helper.KeepOnlyNumbersAndDotInString(name1);
                            name2 = Helper.KeepOnlyNumbersAndDotInString(name2);

                            if (CheckRange1(menuItem, name1) && CheckRange2(menuItem, name2))
                            {
                                menuItem.name = name1;
                                menuItem.name2 = name2;
                            }
                            else
                            {
                                return;
                            }
                                
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    
                    if (menuItem.menuType == MenuType.Gauge)
                    {
                        var digit = float.Parse(menuItem.name);
                        digit += UnityEngine.Random.Range(-200, 201);
                        menuItem.prepend = "(" + digit + ") ";
                    }
                    
                    ClearScratchPad();
                    RefreshScreen();
                    break;
                }
            }
        }

        public bool CheckRange1(MenuItem menuItem, string value)
        {
            float? f = Helper.ParseFloat(value);
            if (f == null)
            {
                DisplayError(Errors.InvalidEntry);
                return false;
            }
            
            if (f < menuItem.minRange || f > menuItem.maxRange)
            {
                DisplayError(Errors.OutOfRange);
                return false;
            }

            return true;
        }
        
        public bool CheckRange2(MenuItem menuItem, string value)
        {
            float? f = Helper.ParseFloat(value);
            if (f == null)
            {
                DisplayError(Errors.InvalidEntry);
                return false;
            }

            if (f < menuItem.minRange2 || f > menuItem.maxRange2)
            {
                DisplayError(Errors.OutOfRange);
                return false;
            }

            return true;
        }

        public void DisplayError(string errorText)
        {
            InputManager.Instance.currentInputState = InputState.Error;
            Debug.Log("displaying error " + errorText);
            SetScratchPadText(errorText);
        }

        public void ClearError()
        {
            InputManager.Instance.currentInputState = InputState.CanEdit;
            ClearScratchPad();
        }

        public void SetTitleText()
        {
            if (currentScreen.titleIsLeftAligned)
            {
                titleLeftTextMesh.text = currentScreen.titleName;
            }
            else
            {
                titleTextMesh.text = currentScreen.titleName;
            }
        }

        public void SetPaginationText()
        {
            paginationTextMesh.text = currentScreen.currentPagination + "/" + currentScreen.totalPagination;
        }

        public void SetAllOtherTexts()
        {
            if (currentScreen.leftTexts != null)
            {
                for (var i = 0; i < currentScreen.leftTexts.Length; i++)
                {
                    var t = leftTextMeshes[i];

                    SetMenuItemText(currentScreen.leftTexts[i], t);
                    
                    switch (currentScreen.leftTexts[i].menuType)
                    {
                        case MenuType.Navigation:
                            if (!string.IsNullOrEmpty(t.text))
                            t.text = Text.LeftCaretSymbol + "<size=\"10%\"> </size>" + t.text;
                            break;
                        case MenuType.Toggle:
                            t.text = RenderToggleField(currentScreen.leftTexts[i]);
                            break;
                    }
                }
            }

            if (currentScreen.rightTexts != null)
            {
                for (var i = 0; i < currentScreen.rightTexts.Length; i++)
                {
                    var t = rightTextMeshes[i];

                    var menuItem = currentScreen.rightTexts[i];
                    
                    // Calculate Sum. Only done for the right side so far.
                    if (menuItem.autoCalculateSum != null && menuItem.autoCalculateSum.Length > 0)
                    {
                        int count = 0;
                        Debug.Log("running");
                        for (var j = 0; j < menuItem.autoCalculateSum.Length; j++)
                        {
                            var r = menuItem.autoCalculateSum[j];
                            
                            MenuItem menuItemToUse = null;
                            if (r < 6)
                            {
                                // then this info is on the left
                                if (currentScreen.leftTexts != null)
                                {
                                    menuItemToUse = currentScreen.leftTexts[j];
                                }
                            }
                            else
                            {
                                var rightAnswer = r - 6;
                                menuItemToUse = currentScreen.rightTexts[rightAnswer];
                            }

                            if (menuItemToUse != null)
                            {
                                var a = int.Parse(menuItemToUse.name);

                                Debug.Log(a);
                                
                                count += a;
                            }  
                        }

                        menuItem.name = count.ToString(CultureInfo.InvariantCulture);
                    }
                    
                    SetMenuItemText(menuItem, t);
                   
                    switch (currentScreen.rightTexts[i].menuType)
                    {
                        case MenuType.Navigation:
                            if (!string.IsNullOrEmpty(t.text))
                            t.text += "<size=\"10%\"> </size>" + Text.RightCaretSymbol;
                            break;
                        case MenuType.Toggle:
                            t.text = RenderToggleField(currentScreen.rightTexts[i]);
                            break;
                    }
                    
                   
                }
            }
            
//            if (menuItem.autoCalculateSum != null && menuItem.autoCalculateSum.Length > 0)
//            {
//                float count = 0;
//                for (var i = 0; i < menuItem.autoCalculateSum.Length; i++)
//                {
//                    var t = menuItem.autoCalculateSum[i];
//                            
//                    MenuItem menuItemToUse = null;
//                    if (t < 6)
//                    {
//                        // then this info is on the left
//                        menuItemToUse = currentScreen.leftTexts[i];
//                    }
//                    else
//                    {
//                        // then this info is on the right
//                        menuItemToUse = currentScreen.rightTexts[i];
//                    }
//
//                    count += float.Parse(menuItemToUse.name);
//                }
//
//                menuItem.name = count.ToString(CultureInfo.InvariantCulture);
//            }
            


            if (currentScreen.leftCaptionTexts != null)
            {
                for (var i = 0; i < currentScreen.leftCaptionTexts.Length; i++)
                {
                    var t = leftCaptionTextMeshes[i];

                    t.text = currentScreen.leftCaptionTexts[i].name;

                    ParseHtmlCodes(t);

                    ChangeColor(currentScreen.leftCaptionTexts[i], t);
                }
            }

            if (currentScreen.rightCaptionTexts != null)
            {
                for (var i = 0; i < currentScreen.rightCaptionTexts.Length; i++)
                {
                    var t = rightCaptionTextMeshes[i];
                    t.text = currentScreen.rightCaptionTexts[i].name;

                    ParseHtmlCodes(t);

                    ChangeColor(currentScreen.rightCaptionTexts[i], t);
                }
            }
        }

        public string RenderToggleField(MenuItem menuItem)
        {
            var activeToggle = menuItem.currentActiveToggle;
            var s = "";
            var hex = "";
            
            var customColor = GlobalSettings.Instance.colors.FirstOrDefault(x =>
                x.name == menuItem.color );

            if (customColor != null)
            {
                hex = ColorUtility.ToHtmlStringRGBA(customColor.color);
            }
            
            for (var i = 0; i < menuItem.toggleFields.Length; i++)
            {
                var t = menuItem.toggleFields[i];

                if (i == activeToggle)
                {
                    t = "<#" + hex + ">" + t;
                }
                else
                {
                    t = "<#FFFFFF>" + t;
                }

                if (i == menuItem.toggleFields.Length -1)
                {
                    s += t;
                }
                else
                {
                    s += t + "<color=\"white\">/</color>";
                }
            }

            return s;
        }

        public void SetMenuItemText(MenuItem menuItem, TextMeshProUGUI t)
        {
            var nameToUse = menuItem.name;
            var nameToUse2 = menuItem.name2;
            
            if (menuItem.decimalPlaces > 0)
            {
                decimal i = decimal.Parse(menuItem.name);
                nameToUse = string.Format(new NumberFormatInfo {NumberDecimalDigits = menuItem.decimalPlaces}, "{0:F}",
                    i);
            }

            if (menuItem.decimalPlaces2 > 0)
            {
                decimal i = decimal.Parse(menuItem.name2);
                nameToUse2 = string.Format(new NumberFormatInfo {NumberDecimalDigits = menuItem.decimalPlaces}, "{0:F}",
                    i);
            }

            t.text = nameToUse;

            if (menuItem.isPrependPositiveNegative)
            {
                var n = float.Parse(nameToUse) >= 0 ? "+" : "";
                t.text = n + t.text;
            }

            if (!menuItem.prepend.IsNullOrWhitespace())
            {
                t.text = menuItem.prepend + t.text;
            }

            if (!menuItem.append.IsNullOrWhitespace())
            {
                t.text += menuItem.append;
            }

            if (!menuItem.name2.IsNullOrWhitespace())
            {
                if (!menuItem.prepend2.IsNullOrWhitespace())
                {
                    t.text += menuItem.prepend2;
                }

                t.text += nameToUse2;

                if (!menuItem.append2.IsNullOrWhitespace())
                {
                    t.text += menuItem.append2;
                }
            }

            if (menuItem.isAppendTemperatureSign)
            {
                t.text += Text.TemperatureSign;
            }

            ParseHtmlCodes(t);

            ChangeColor(menuItem, t);
           
        }

        public void ParseHtmlCodes(TextMeshProUGUI textMesh)
        {
            if (string.IsNullOrEmpty(textMesh.text))
                return;

            textMesh.text = textMesh.text.Replace("&nbsp;", ((char) 160).ToString());
        }

        public void ChangeColor(MenuItem menuItem, TextMeshProUGUI t)
        {
            if (menuItem.color != null)
            {
                var customColor = GlobalSettings.Instance.colors.FirstOrDefault(x =>
                    x.name == menuItem.color);

                if (customColor != null)
                {
                    var hex = ColorUtility.ToHtmlStringRGBA(customColor.color);
                    t.text = "<#" + hex + ">" + t.text;
                }
            }
        }
    }
}