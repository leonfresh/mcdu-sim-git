﻿using System;
using System.IO;
using Boo.Lang;
using Newtonsoft.Json;
using UnityEngine;

namespace Managers
{
    public class JsonManager : Singleton<JsonManager>
    {
        [SerializeField]
        public List<ScreenJson> screenJsons = new List<ScreenJson>();
        
        private void Awake()
        {
            StreamReader file = File.OpenText(Application.streamingAssetsPath + "/screens.json");
            screenJsons = JsonConvert.DeserializeObject<List<ScreenJson>>(file.ReadToEnd());
        }
    }

    [Serializable]
    public class ScreenJson
    {
        public string screenName;
        public string titleName;
        public int currentPagination;
        public int totalPagination;
        public bool titleIsLeftAligned;

        public MenuItem[] leftTexts;
        public MenuItem[] rightTexts;

        public MenuItem[] leftCaptionTexts;
        public MenuItem[] rightCaptionTexts;
    }

    [Serializable]
    public class MenuItem
    {
        public string name;
        public string name2;
        
        public MenuType menuType = MenuType.Text;
        public string color;
        
        public string prepend;
        public string prepend2;
        
        public string append;
        public string append2;
        
        public int charLimit;
        
        public double minRange;
        public double maxRange;
        public double minRange2;
        public double maxRange2;

        public int decimalPlaces;
        public int decimalPlaces2;
        
        public bool isAppendTemperatureSign;
        public bool isPrependPositiveNegative;

        public bool isInvertIfCustomValue;
        public int[] autoCalculateSum;
        // If auto calculate is out of range, then it will make an inverted colour.

        public bool isInvertIfOutOfRange;

        public string[] toggleFields;

        public string dropdownTitle;
        public string[] dropdowns;

        public bool isDropdown;
        
        [Header("Non Json Fields")]
        public int currentActiveToggle = 0;
    }

    public enum MenuType
    {
        Text = 0,
        Navigation = 1,
        EditableNumber = 2,
        EditableNumberAlphabet = 3,
        Toggle = 4,
        Gauge = 5
    }
}