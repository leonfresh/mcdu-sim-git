using System;
using System.Collections.Generic;
using System.Linq;

namespace Managers
{
    public enum ButtonType
    {
        None = 0,
        LineKeys = 1,
        FunctionKeys = 2,
        AlphaKeys = 3,
        NumericKeys = 4,
        PrevNextKeys = 5
    }

    public enum InputState
    {
        CanEdit = 0,
        Error = 1
    }
    
    public class InputManager : Singleton<InputManager>
    {        
        public List<string> keysHeldDown = new List<string>();
        public float keyHoldDownRepeatTime = 0.5f;

        public InputState currentInputState;
        
        public void HandleInputDown(string buttonName, ButtonType buttonType)
        {
            switch (buttonType)
            {
                case ButtonType.AlphaKeys:
                case ButtonType.NumericKeys:
                    if (currentInputState == InputState.Error && buttonName == "DEL")
                    {
                        DisplayManager.Instance.ClearError();
                    }
                    else if (currentInputState == InputState.CanEdit)
                    {
                        DisplayManager.Instance.RenderScratchPad(buttonName);
                    }
                    break;
                case ButtonType.LineKeys:
                    break;
                case ButtonType.FunctionKeys:
                    break;
                case ButtonType.None:
                    break;
                case ButtonType.PrevNextKeys:
                    break;
                default:
                    throw new ArgumentOutOfRangeException("buttonType", buttonType, null);
            }
        }
        
        public void HandleInputUp(string buttonName, ButtonType buttonType)
        {
            switch (buttonType)
            {
                case ButtonType.LineKeys:
                    DisplayManager.Instance.RenderScreen(buttonName, buttonType);
                    break;
                case ButtonType.FunctionKeys:
                   DisplayManager.Instance.RenderScreen(buttonName, buttonType);
                    break;
                case ButtonType.AlphaKeys:
                case ButtonType.NumericKeys:
//                    keysHeldDown.RemoveAll(x => keysHeldDown[0].ToString().Equals(buttonName));
                    keysHeldDown.RemoveAll(x => x.Split(',').First().Equals(buttonName));
//                    keysHeldDown.Remove(buttonName);
                    break;
                default:
                    if (buttonName == Key.Prev)
                    {
                        if (DisplayManager.Instance.currentScreen != null &&
                            DisplayManager.Instance.currentScreen.screenName.Length > 0)
                            DisplayManager.Instance.RenderScreen(DisplayManager.Instance.currentScreen.screenName,
                                buttonType, -1);
                    }
                    else if (buttonName == Key.Next)
                    {
                        if (DisplayManager.Instance.currentScreen != null &&
                            DisplayManager.Instance.currentScreen.screenName.Length > 0)
                            DisplayManager.Instance.RenderScreen(DisplayManager.Instance.currentScreen.screenName,
                                buttonType, 1);
                    }
                    break;
            }
        }
    }
}