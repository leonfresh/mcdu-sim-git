using System.Globalization;
using System.Text.RegularExpressions;

namespace Managers
{
    public static class Helper
    {
        public static string KeepOnlyNumbersAndDotInString(string value)
        {
            return Regex.Replace(value, "[^0-9.-]", "");
        }

        public static float? ParseFloat(string value)
        {
            float f;
            bool success = float.TryParse(value, NumberStyles.Any, new CultureInfo("en-us"), out f);
            if (!success)
            {
                return null;
            }

            return f;
        }
    }
}