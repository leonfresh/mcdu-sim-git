using System;
using Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Handlers
{
    public class ButtonClickHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public Sprite offSprite;
        public Sprite onSprite;
        private Button _button;
        private Image _buttonLetterImage;

        public float buttonLetterOffset;
    
        public ButtonType buttonType;
    
        private float _buttonLetterInitialOffset;

        private void Awake()
        {
            if (transform.childCount > 0)
            {
                _buttonLetterImage = transform.GetChild(0).GetComponent<Image>();
                _buttonLetterInitialOffset = _buttonLetterImage.transform.localPosition.y;
            }

            _button = GetComponent<Button>();
        }

        public void ChangeImage()
        {
            var image = _button.image;
            image.sprite = image.sprite == onSprite ? offSprite : onSprite;

            if (_buttonLetterImage != null)
            {
                var trans = _buttonLetterImage.transform.localPosition;

                if (Math.Abs(trans.y - _buttonLetterInitialOffset) < 0.1f)
                {
                    _buttonLetterImage.transform.Translate(0,buttonLetterOffset,0,Space.Self);
                }
                else
                {
                    _buttonLetterImage.transform.Translate(0,-buttonLetterOffset,0,Space.Self);
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            InputManager.Instance.HandleInputDown(name, buttonType);
        
            ChangeImage();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Debug.Log(name + " clicked");

//        DisplayManager.Instance.RenderScreen(name, buttonChangesScreen);
        
            InputManager.Instance.HandleInputUp(name, buttonType);
        
            ChangeImage();
        }
    }
}
