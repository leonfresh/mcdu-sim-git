﻿using TMPro;
using UnityEngine;

namespace Handlers
{
	public class SpaceReplacer : MonoBehaviour {

		private void Awake()
		{
			var text = GetComponent<TextMeshProUGUI>();
			text.text = text.text.Replace("&nbsp;", ((char) 160).ToString() );
		}
	}
}
