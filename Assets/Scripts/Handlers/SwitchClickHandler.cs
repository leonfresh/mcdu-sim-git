using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Handlers
{
    public class SwitchClickHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public Sprite upSprite;
        public Sprite downSprite;
        public Sprite defaultSprite;
        public Image switchBackground;
        private Image _buttonLetterImage;

        public float buttonLetterOffset;
    
        private float _buttonLetterInitialOffset;

        private void Awake()
        {
            if (transform.childCount > 0)
            {
                _buttonLetterImage = transform.GetChild(0).GetComponent<Image>();
                _buttonLetterInitialOffset = _buttonLetterImage.transform.localPosition.y;
            }
        }

        public void ChangeImage(string action = "")
        {
            if (action.Length > 0)
            {
                Debug.Log(action + " clicked");
            }
            switch (action)
            {
                case "BRT":
                    switchBackground.sprite = upSprite;
                    break;
                case "DIM":
                    switchBackground.sprite = downSprite;
                    break;
                default:
                    switchBackground.sprite = defaultSprite;
                    break;
            }
        
            if (_buttonLetterImage != null)
            {
                var trans = _buttonLetterImage.transform.localPosition;

                if (Math.Abs(trans.y - _buttonLetterInitialOffset) < 0.1f)
                {
                    _buttonLetterImage.transform.Translate(0,buttonLetterOffset,0,Space.Self);
                }
                else
                {
                    _buttonLetterImage.transform.Translate(0,-buttonLetterOffset,0,Space.Self);
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            ChangeImage(name);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ChangeImage();
        }
    }
}
